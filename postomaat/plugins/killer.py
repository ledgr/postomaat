# -*- coding: UTF-8 -*-
#   Copyright 2012-2018 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#

from postomaat.shared import ScannerPlugin, DISCARD


class KillerPlugin(ScannerPlugin):
    """DISCARD all mails (for special setups, debugging, testing)"""
    
    def __init__(self, config, section=None):
        ScannerPlugin.__init__(self, config, section)
        self.logger = self._logger()

    def examine(self, suspect):
        return DISCARD

    def lint(self):
        print("""!!! WARNING: You have enabled the KILLER plugin - ALL messages will be discarded. !!!""")
        return True

    def __str__(self):
        return "Discard Messages"
