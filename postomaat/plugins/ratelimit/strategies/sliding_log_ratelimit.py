# -*- coding: UTF-8 -*-
import sys
import time
from collections import defaultdict
from hashlib import md5
from threading import Lock

from postomaat.extensions.sql import SQL_EXTENSION_ENABLED, get_session

REDIS_AVAILABLE = 0
try:
    import redis
    REDIS_AVAILABLE = 1
except ImportError:
    pass

if sys.version_info > (3,):
    unicode = str # pylint: disable=C0103

STRATEGY = 'sliding-log'
BACKENDS = defaultdict(dict)

__all__ = ['STRATEGY', 'BACKENDS']


class RollingWindowRatelimit(object):
    def __init__(self, backendconfig):
        pass

    def _fix_eventname(self, eventname):
        if not isinstance(eventname, unicode):
            eventname = unicode(eventname)
        if len(eventname) > 255:
            eventname = unicode(md5(eventname).hexdigest())
        return eventname
        
    def check_count(self, eventname, timediff):
        pass

    def check_allowed(self, eventname, limit, timediff):
        pass

    def add(self, eventname):
        pass

    def clear(self, eventname, abstime=None):
        pass

    def count(self, eventname):
        pass


class MemoryBackend(RollingWindowRatelimit):
    def __init__(self, backendconfig):
        super(MemoryBackend, self).__init__(backendconfig)
        self.memdict = defaultdict(list)
        self.lock = Lock()

    def check_count(self, eventname, timediff):
        """record a event. Returns the current count"""
        now = self.add(eventname)
        then = now-timediff
        self.clear(eventname, then)
        self.add(eventname)
        count = self.count(eventname)
        return count

    def add(self, eventname):
        """add a tick to the event and return its timestamp"""
        now = time.time()
        self.lock.acquire()
        self.memdict[eventname].append(now)
        self.lock.release()
        return now

    def clear(self, eventname, abstime=None):
        """
        clear events before abstime in secs
        if abstime is not provided, clears the whole queue
        """
        if abstime is None:
            abstime = int(time.time())
            
        if eventname not in self.memdict:
            return

        self.lock.acquire()
        try:
            while self.memdict[eventname][0] < abstime:
                del self.memdict[eventname][0]
        except IndexError: #empty list, remove
            del self.memdict[eventname]

        self.lock.release()

    def count(self, eventname):
        self.lock.acquire()
        try:
            count = len(self.memdict[eventname])
        except KeyError:
            count = 0
        self.lock.release()
        return count

    def check_allowed(self, eventname, limit, timediff):
        count = self.check_count(eventname, timediff)
        return count <= limit, count

BACKENDS[STRATEGY]['memory'] = MemoryBackend


if REDIS_AVAILABLE:
    class RedisBackend(RollingWindowRatelimit):
        def __init__(self, backendconfig):
            super(RedisBackend, self).__init__(backendconfig)
            self.redis = redis.StrictRedis.from_url(backendconfig)

        def count(self, eventname, timespan):
            now = time.time()
            then = now-timespan
            if then is None:
                then = int(time.time())

            pipe = self.redis.pipeline()
            pipe.zadd(eventname, now, now)
            pipe.zremrangebyscore(eventname, '-inf', then)
            pipe.zcard(eventname)
            return pipe.execute()[2]

        def check_allowed(self, eventname, limit, timespan):
            eventname = self._fix_eventname(eventname)
            count = self.count(eventname, timespan)
            return count <= limit, count

    BACKENDS[STRATEGY]['redis'] = RedisBackend


if SQL_EXTENSION_ENABLED:
    from sqlalchemy import Column, Integer, Unicode, BigInteger, Index
    from sqlalchemy.sql import and_
    from sqlalchemy.ext.declarative import declarative_base
    DeclarativeBase = declarative_base()
    metadata = DeclarativeBase.metadata

    class Event(DeclarativeBase):
        __tablename__ = 'postomaat_ratelimit_rolling_window'
        eventid = Column(BigInteger, primary_key=True)
        eventname = Column(Unicode(255), nullable=False)
        occurence = Column(Integer, nullable=False)
        __table_args__ = (
            Index('idx_eventname', 'eventname'),
        )

    class SQLAlchemyBackend(RollingWindowRatelimit):
        def __init__(self, backendconfig):
            super(SQLAlchemyBackend, self).__init__(backendconfig)
            self.session = get_session(backendconfig)
            metadata.create_all(bind=self.session.bind)

        def db_add(self, eventname, timestamp):
            event = Event()
            event.eventname = eventname
            event.occurence = int(timestamp)
            self.session.add(event)
            self.session.flush()

        def db_clear(self, eventname, abstime):
            self.session.query(Event).filter(
                and_(Event.eventname == eventname, Event.occurence < abstime)
            ).delete()
            self.session.flush()

        def db_count(self, eventname):
            return self.session.query(Event).filter(Event.eventname == eventname).count()

        def count(self, eventname, timespan):
            now = time.time()
            then = now-timespan
            if then is None:
                then = int(time.time())

            self.db_add(eventname, now)
            self.db_clear(eventname, then)
            return self.db_count(eventname)
            
        def check_allowed(self, eventname, limit, timespan):
            eventname = self._fix_eventname(eventname)
            count = self.count(eventname, timespan)
            return count <= limit, count

    BACKENDS[STRATEGY]['sqlalchemy'] = SQLAlchemyBackend
