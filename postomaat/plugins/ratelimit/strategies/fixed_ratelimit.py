# -*- coding: UTF-8 -*-
import sys
import time
from collections import defaultdict
from hashlib import md5
from threading import Lock

from postomaat.extensions.sql import SQL_EXTENSION_ENABLED, get_session

REDIS_AVAILABLE = 0
try:
    import redis
    REDIS_AVAILABLE = 1
except ImportError:
    pass

if sys.version_info > (3,):
    unicode = str # pylint: disable=C0103

STRATEGY = 'fixed'
BACKENDS = defaultdict(dict)

__all__ = ['STRATEGY', 'BACKENDS']


class FixedRatelimit(object):
    def __init__(self, backendconfig):
        pass

    def _fix_eventname(self, eventname):
        if not isinstance(eventname, unicode):
            eventname = unicode(eventname)
        if len(eventname) > 255:
            eventname = unicode(md5(eventname).hexdigest())
        return eventname

    def inc(self, eventname):
        pass

    def count(self, eventname):
        pass

    def check_allowed(self, eventname, limit, timespan):
        return True


class MemoryBackend(FixedRatelimit):
    def __init__(self, config):
        super(MemoryBackend, self).__init__(config)
        self.memdict = defaultdict(lambda: {'count': 0, 'name': str})
        self.lock = Lock()

    def expire(self, eventname, abstime):
        self.lock.acquire()
        try:
            if self.memdict[eventname]['timestamp'] < abstime:
                del self.memdict[eventname]
        except KeyError:
            pass
        self.lock.release()

    def increment(self, eventname, timestamp):
        self.lock.acquire()
        self.memdict[eventname]['timestamp'] = timestamp
        self.memdict[eventname]['count'] += 1
        self.lock.release()

    def count(self, eventname):
        self.lock.acquire()
        try:
            count = self.memdict[eventname]['count']
        except KeyError:
            count = 0
        self.lock.release()
        return count

    def check_allowed(self, eventname, limit, timespan):
        # TODO: expire not touched events (stale)
        now = time.time()
        then = now - timespan
        self.expire(eventname, then)
        self.increment(eventname, now)
        count = self.count(eventname)
        return count <= limit, count


BACKENDS[STRATEGY]['memory'] = MemoryBackend


if REDIS_AVAILABLE:
    class RedisBackend(FixedRatelimit):
        def __init__(self, backendconfig):
            super(RedisBackend, self).__init__(backendconfig)
            self.redis = redis.StrictRedis.from_url(backendconfig)

        def increment(self, eventname, timespan):
            eventname = self._fix_eventname(eventname)
            pipe = self.redis.pipeline()
            pipe.incr(eventname)
            pipe.expire(eventname, timespan)
            return pipe.execute()[0]

        def check_allowed(self, eventname, limit, timespan):
            count = self.increment(eventname, timespan)
            return count <= limit, count

    BACKENDS[STRATEGY]['redis'] = RedisBackend


if SQL_EXTENSION_ENABLED:
    from sqlalchemy import Column, Integer, Unicode, BigInteger, Index, DateTime
    from sqlalchemy.sql import and_
    from sqlalchemy.ext.declarative import declarative_base
    DeclarativeBase = declarative_base()
    metadata = DeclarativeBase.metadata

    class Event(DeclarativeBase):
        __tablename__ = 'postomaat_ratelimit_fixed'
        eventid = Column(BigInteger, primary_key=True)
        eventname = Column(Unicode(255), nullable=False)
        count = Column(Integer, nullable=False)
        timestamp = Column(Integer, nullable=False)
        __table_args__ = (
            Index('idx_eventname', 'eventname'),
            Index('idx_timestamp', 'timestamp'),
        )

    class SQLAlchemyBackend(FixedRatelimit):
        def __init__(self, backendconfig):
            super(SQLAlchemyBackend, self).__init__(backendconfig)
            self.session = get_session(backendconfig)
            metadata.create_all(bind=self.session.bind)

        def expire(self, eventname, abstime):
            self.session.query(Event).filter(
                and_(Event.eventname == eventname, Event.timestamp < abstime)
            ).delete()
            self.session.flush()

        def increment(self, eventname, timestamp):
            event = Event()

            instance = self.session.query(Event).filter(Event.eventname == eventname).first()
            if instance is None:
                # if not exists - create
                event.eventname = eventname
                event.count = 1
                event.timestamp = int(timestamp)
                self.session.add(event)
            else:
                # if exists - increment
                instance.count = instance.count + 1
                instance.timestamp = int(timestamp)

            self.session.flush()

        def db_count(self, eventname):
            e = self.session.query(Event.count).filter(Event.eventname == eventname).first()
            if e is None:
                return 0
            return e.count

        def count(self, eventname, timespan):
            now = time.time()
            then = now-timespan
            if then is None:
                then = int(time.time())

            self.expire(eventname, then)
            self.increment(eventname, now)
            return self.db_count(eventname)
            
        def check_allowed(self, eventname, limit, timespan):
            eventname = self._fix_eventname(eventname)
            count = self.count(eventname, timespan)
            return count <= limit, count

    BACKENDS[STRATEGY]['sqlalchemy'] = SQLAlchemyBackend
