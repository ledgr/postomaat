# -*- coding: UTF-8 -*-
import sys
import time
import math
from collections import defaultdict
from hashlib import md5

from postomaat.shared import ScannerPlugin

REDIS_AVAILABLE = 0
try:
    import redis
    REDIS_AVAILABLE = 1
except ImportError:
    pass

if sys.version_info > (3,):
    unicode = str # pylint: disable=C0103

STRATEGY = 'sliding-window'
BACKENDS = defaultdict(dict)

__all__ = ['STRATEGY', 'BACKENDS']


class SlidingWindowRatelimit(ScannerPlugin):
    """ This strategy is based on the blog post by CloudFlare
    https://blog.cloudflare.com/counting-things-a-lot-of-different-things

    I hope I got this right

    Basically we have two buckets - past and present
    When we are calculating the rate, we take percentage of previous bucket
    and add the total amount of present bucket.
    This way we have quite good approximation of the rate.

    This algorithm:
      - requires less memory than sliding-log algorithm
      - doesn't require expensive(?) operation of old data cleanup
        like sliding-log does
      - avoids double-burst problem of fixed ratelimit algorithm
      - BUT is less atomic, so less precise
      - uses more memory than fixed ratelimit algorithm

    TODO:
        - add async updates to redis
        - avoid race conditions if any (?)
        - improve performance (?)
    """
    def __init__(self, backendconfig):
        pass

    def _fix_eventname(self, eventname):
        if not isinstance(eventname, unicode):
            eventname = unicode(eventname)
        if len(eventname) > 255:
            eventname = unicode(md5(eventname).hexdigest())
        return eventname

    def check_allowed(self, eventname, limit, timespan):
        return True


if REDIS_AVAILABLE:
    class RedisBackend(SlidingWindowRatelimit):
        def __init__(self, backendconfig):
            super(RedisBackend, self).__init__(backendconfig)
            self.redis = redis.StrictRedis.from_url(backendconfig)

        def add(self, eventname, ttl=0):
            event_data = {
                'mitigate': 0,
                'bucket0': 0,
                'bucket1': 0,
                'last_bucket': self.present_bucket,
                'bucket_start_ts': self.now
            }
            pipe = self.redis.pipeline()
            pipe.hmset(eventname, event_data)
            pipe.expire(eventname, ttl)
            pipe.execute()

        def get_event(self, eventname):
            return self.redis.hgetall(eventname)

        def update(self, eventname, event_data):
            self.redis.hmset(eventname, event_data)

        def set_mitigate(self, event, retry_after):
            event.update({
                'mitigate': float(self.now) + float(retry_after)
            })

        def get_buckets(self, timespan):
            """get time buckets where counters are saved
            we have two buckets only, but this formula can generate multiple
            math.floor((time_now / measurement_timespan) / bucket_interval)
            """
            present_bucket = int(math.floor((self.now % (timespan * 2)) / timespan))
            past_bucket = 1 - present_bucket
            return "bucket" + str(present_bucket), "bucket" + str(past_bucket)

        def reset_buckets(self, event):
            event.update({
                'bucket0': 0,
                'bucket1': 0,
                'last_bucket': self.present_bucket,
                'bucket_start_ts': self.now
            })

        def reset_bucket(self, event, bucket):
            event[bucket] = 0

        def increment(self, event):
            event[self.present_bucket] = int(event[self.present_bucket]) + 1

        def change_bucket(self, event):
            event.update({
                'last_bucket': self.present_bucket,
                'bucket_start_ts': self.now
            })

        def count(self, event, timespan):
            t_into_bucket = self.now - float(event['bucket_start_ts'])
            present_b = self.present_bucket # present bucket count
            past_b = self.past_bucket       # past bucket count

            count = math.ceil(
                int(event[past_b]) * ((timespan - t_into_bucket) / timespan) + int(event[present_b]) # pylint: disable=C0301
            )
            return count

        def check_allowed(self, eventname, limit, timespan):
            self.now = time.time()
            self.present_bucket, self.past_bucket = self.get_buckets(timespan)
            count = -1 # not calculated yet or mitigation is on

            event = self.get_event(eventname)
            if not event:
                self.add(eventname, ttl=timespan * 3)
                event = self.get_event(eventname)

            # we are ahead of both bucket timespans
            # so the counters are irrelevant and must be reset
            if float(event['bucket_start_ts']) + float(2 * timespan) < self.now:
                self.reset_buckets(event)

            if self.present_bucket != event['last_bucket']:
                self.change_bucket(event)
                self.reset_bucket(event, self.present_bucket)
                self.redis.expire(eventname, timespan * 3)

            if 'mitigate' in event and float(event['mitigate']) > self.now:
                return False, count

            count = self.count(event, timespan) + 1 # +1 because we check if we WOULD allow
            if count >= limit:
                try:
                    retry_after = float(timespan)/float(event[self.past_bucket])
                    self.set_mitigate(event, retry_after)
                except ZeroDivisionError:
                    retry_after = 0
                self.set_mitigate(event, retry_after)
                self.update(eventname, event)
                return False, count

            self.increment(event)
            self.update(eventname, event)

            return True, count

    BACKENDS[STRATEGY]['redis'] = RedisBackend
