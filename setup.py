import os
import glob
from setuptools import find_packages, setup

# Dynamically calculate the version based on postomaat.VERSION.
VERSION = __import__('postomaat').get_version()

def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as handler:
        return handler.read()

setup(
    name="postomaat",
    version=VERSION,
    description="Postomaat Policy Daemon",
    author="O. Schacher",
    url='https://gitlab.com/fumail/postomaat',
    download_url='https://gitlab.com/fumail/postomaat/-/archive/master/postomaat-master.tar.bz2',
    author_email="oli@fuglu.org",
    packages=find_packages(),
    scripts=['postomaat/bin/postomaat_conf'],
    entry_points={'console_scripts': [
        'postomaat = postomaat.service:main',
    ]},
    long_description=read('README.md'),
    include_package_data=True,
    install_requires=[
        'importlib;python_version>"2,7"',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Topic :: Communications :: Email',
        'Topic :: Communications :: Email :: Filters',
    ],
    data_files=[
        ('/etc/postomaat', glob.glob('conf/*.dist')),
        ('/etc/postomaat/conf.d', glob.glob('conf/conf.d/*.dist')),
    ],
)
